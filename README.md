# GitLab CI/CD Pipeline: Docker Build

This project provides a template for the `.gitlab-ci.yml` file. The template can be imported and
overridden by individual projects without modifying the original configuration.

Unless testing a future version, projects should import a tagged version of this template.

Documentation and examples on how to use this feature can be found here: https://docs.gitlab.com/ee/ci/yaml/#include

This particular template is intended for projects that need to build docker images.

In order to use this template, copy the following into the project's `.gitlab-ci.yml`.

```
include:
  - remote: "https://gitlab.com/byuhbll/lib/gitlab-ci/docker-build/raw/1.0.0/template.yml"
```

## Ya, but what does it do?

This template provides three `build` stage jobs that will get run under different situations and will tag the resulting docker image slightly differently. All of these jobs, when triggered, will build a docker image based on the commit that triggered the build and will upload the resulting image to the project's container registry.

Each project is listed below with the conditions that will trigger it and the tag that is used for the resulting docker image:

* **prerelease**: Run on all *non-master branches*. Tag format: `$CI_REGISTRY_IMAGE/prerelease:$CI_COMMIT_REF_SLUG` (ex. registry.gitlab.com/byuhbll/apps/myapplication/prerelease:mybranch)
* **release**: Run on all tags matching `#.#.#`. Tag format: `$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG` (ex. registry.gitlab.com/byuhbll/apps/myapplication:1.0.0)
* **release-latest**: Run on master. Tag format: `$CI_REGISTRY_IMAGE:latest` (ex. registry.gitlab.com/byuhbll/apps/myapplication:latest)